﻿using Dapper;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using PretestCoreSurya.Model;
using PretestCoreSurya.Repository.Interface;
using System.Data;

namespace PretestCoreSurya.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class CompanyController : Controller
    {
        private readonly ICompany _authentication;
        public CompanyController(ICompany authentication)
        {
            _authentication = authentication;
        }

        [HttpGet("CompanyList")]
        [Authorize(Roles = "Admin")]
        public IActionResult getUser()
        {
            var result = _authentication.getCompanyList<ModelCompany>();

            return Ok(result);
        }

        [HttpPost("Create")]
        [Authorize(Roles = "Admin")]
        public IActionResult CreateCompany([System.Web.Http.FromBody] ModelCompany company)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.String);
            dp_param.Add("createdBy", company.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_CreateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpPut("Update")]
        [Authorize(Roles = "Admin")]
        public IActionResult UpdateCompany([System.Web.Http.FromBody] ModelCompany company, string id)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("name", company.Name, DbType.String);
            dp_param.Add("address", company.Address, DbType.String);
            dp_param.Add("email", company.Email, DbType.String);
            dp_param.Add("telephone", company.Telephone, DbType.String);
            dp_param.Add("flag", company.Flag, DbType.String);
            dp_param.Add("createdBy", company.CreatedBy, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_UpdateCompany", dp_param);
            if (result.Code == 200)
            {
                return Ok(result);
            }

            return BadRequest(result);
        }

        [HttpDelete("Delete")]
        [Authorize(Roles = "Admin")]
        public IActionResult DeleteCompany(string id)
        {
            if (id == string.Empty)
            {
                return BadRequest("Parameter is missing");
            }

            DynamicParameters dp_param = new DynamicParameters();
            dp_param.Add("ID", id, DbType.String);
            dp_param.Add("retVal", DbType.String, direction: ParameterDirection.Output);

            var result = _authentication.Execute_Command<ModelCompany>("sp_DeleteCompany", dp_param);

            if (result.Code == 200)
            {
                return Ok(result);
            }

            return NotFound(result);
        }


    }
}
