USE [master]
GO
/****** Object:  Database [PretestSurya]    Script Date: 04/08/2023 08:56:21 ******/
CREATE DATABASE [PretestSurya]
 CONTAINMENT = NONE
 ON  PRIMARY 
( NAME = N'PretestSurya', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\PretestSurya.mdf' , SIZE = 8192KB , MAXSIZE = UNLIMITED, FILEGROWTH = 65536KB )
 LOG ON 
( NAME = N'PretestSurya_log', FILENAME = N'C:\Program Files\Microsoft SQL Server\MSSQL16.SQLEXPRESS\MSSQL\DATA\PretestSurya_log.ldf' , SIZE = 8192KB , MAXSIZE = 2048GB , FILEGROWTH = 65536KB )
 WITH CATALOG_COLLATION = DATABASE_DEFAULT, LEDGER = OFF
GO
ALTER DATABASE [PretestSurya] SET COMPATIBILITY_LEVEL = 160
GO
IF (1 = FULLTEXTSERVICEPROPERTY('IsFullTextInstalled'))
begin
EXEC [PretestSurya].[dbo].[sp_fulltext_database] @action = 'enable'
end
GO
ALTER DATABASE [PretestSurya] SET ANSI_NULL_DEFAULT OFF 
GO
ALTER DATABASE [PretestSurya] SET ANSI_NULLS OFF 
GO
ALTER DATABASE [PretestSurya] SET ANSI_PADDING OFF 
GO
ALTER DATABASE [PretestSurya] SET ANSI_WARNINGS OFF 
GO
ALTER DATABASE [PretestSurya] SET ARITHABORT OFF 
GO
ALTER DATABASE [PretestSurya] SET AUTO_CLOSE OFF 
GO
ALTER DATABASE [PretestSurya] SET AUTO_SHRINK OFF 
GO
ALTER DATABASE [PretestSurya] SET AUTO_UPDATE_STATISTICS ON 
GO
ALTER DATABASE [PretestSurya] SET CURSOR_CLOSE_ON_COMMIT OFF 
GO
ALTER DATABASE [PretestSurya] SET CURSOR_DEFAULT  GLOBAL 
GO
ALTER DATABASE [PretestSurya] SET CONCAT_NULL_YIELDS_NULL OFF 
GO
ALTER DATABASE [PretestSurya] SET NUMERIC_ROUNDABORT OFF 
GO
ALTER DATABASE [PretestSurya] SET QUOTED_IDENTIFIER OFF 
GO
ALTER DATABASE [PretestSurya] SET RECURSIVE_TRIGGERS OFF 
GO
ALTER DATABASE [PretestSurya] SET  DISABLE_BROKER 
GO
ALTER DATABASE [PretestSurya] SET AUTO_UPDATE_STATISTICS_ASYNC OFF 
GO
ALTER DATABASE [PretestSurya] SET DATE_CORRELATION_OPTIMIZATION OFF 
GO
ALTER DATABASE [PretestSurya] SET TRUSTWORTHY OFF 
GO
ALTER DATABASE [PretestSurya] SET ALLOW_SNAPSHOT_ISOLATION OFF 
GO
ALTER DATABASE [PretestSurya] SET PARAMETERIZATION SIMPLE 
GO
ALTER DATABASE [PretestSurya] SET READ_COMMITTED_SNAPSHOT OFF 
GO
ALTER DATABASE [PretestSurya] SET HONOR_BROKER_PRIORITY OFF 
GO
ALTER DATABASE [PretestSurya] SET RECOVERY SIMPLE 
GO
ALTER DATABASE [PretestSurya] SET  MULTI_USER 
GO
ALTER DATABASE [PretestSurya] SET PAGE_VERIFY CHECKSUM  
GO
ALTER DATABASE [PretestSurya] SET DB_CHAINING OFF 
GO
ALTER DATABASE [PretestSurya] SET FILESTREAM( NON_TRANSACTED_ACCESS = OFF ) 
GO
ALTER DATABASE [PretestSurya] SET TARGET_RECOVERY_TIME = 60 SECONDS 
GO
ALTER DATABASE [PretestSurya] SET DELAYED_DURABILITY = DISABLED 
GO
ALTER DATABASE [PretestSurya] SET ACCELERATED_DATABASE_RECOVERY = OFF  
GO
ALTER DATABASE [PretestSurya] SET QUERY_STORE = ON
GO
ALTER DATABASE [PretestSurya] SET QUERY_STORE (OPERATION_MODE = READ_WRITE, CLEANUP_POLICY = (STALE_QUERY_THRESHOLD_DAYS = 30), DATA_FLUSH_INTERVAL_SECONDS = 900, INTERVAL_LENGTH_MINUTES = 60, MAX_STORAGE_SIZE_MB = 1000, QUERY_CAPTURE_MODE = AUTO, SIZE_BASED_CLEANUP_MODE = AUTO, MAX_PLANS_PER_QUERY = 200, WAIT_STATS_CAPTURE_MODE = ON)
GO
USE [PretestSurya]
GO
/****** Object:  Table [dbo].[Table_Company]    Script Date: 04/08/2023 08:56:21 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Table_Company](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Table_Company] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Table_Position]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Table_Position](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Table_Position] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Table_User]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Table_User](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDPosition] [int] NULL,
	[Name] [varchar](255) NULL,
	[Address] [text] NULL,
	[Email] [varchar](50) NULL,
	[Telephone] [varchar](14) NULL,
	[Username] [varchar](50) NULL,
	[Password] [varchar](255) NULL,
	[Role] [varchar](50) NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Table_User] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[View_User]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_User]
AS
SELECT        dbo.Table_User.IDCompany, dbo.Table_Company.Name AS CompanyName, dbo.Table_User.IDPosition, dbo.Table_Position.Name AS PositionName, dbo.Table_User.Name, dbo.Table_User.Address, dbo.Table_User.Email, 
                         dbo.Table_User.Telephone, dbo.Table_User.Username, dbo.Table_User.Password, dbo.Table_User.Role, dbo.Table_User.Flag, dbo.Table_User.CreatedBy, dbo.Table_User.CreatedAt
FROM            dbo.Table_Company INNER JOIN
                         dbo.Table_User ON dbo.Table_Company.ID = dbo.Table_User.IDCompany INNER JOIN
                         dbo.Table_Position ON dbo.Table_User.IDPosition = dbo.Table_Position.ID
GO
/****** Object:  Table [dbo].[Table_DocumentCategory]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Table_DocumentCategory](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[Name] [varchar](255) NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Table_DocumentCategory] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  Table [dbo].[Table_Document]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
SET ANSI_PADDING ON
GO
CREATE TABLE [dbo].[Table_Document](
	[ID] [int] IDENTITY(1,1) NOT NULL,
	[UID] [uniqueidentifier] NULL,
	[IDCompany] [int] NULL,
	[IDCategory] [int] NULL,
	[Name] [varchar](255) NULL,
	[Description] [text] NULL,
	[Flag] [int] NULL,
	[CreatedBy] [int] NULL,
	[CreatedAt] [datetime] NULL,
 CONSTRAINT [PK_Table_Document] PRIMARY KEY CLUSTERED 
(
	[ID] ASC
)WITH (PAD_INDEX = OFF, STATISTICS_NORECOMPUTE = OFF, IGNORE_DUP_KEY = OFF, ALLOW_ROW_LOCKS = ON, ALLOW_PAGE_LOCKS = ON, OPTIMIZE_FOR_SEQUENTIAL_KEY = OFF) ON [PRIMARY]
) ON [PRIMARY] TEXTIMAGE_ON [PRIMARY]
GO
SET ANSI_PADDING OFF
GO
/****** Object:  View [dbo].[View_Document]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE VIEW [dbo].[View_Document]
AS
SELECT        dbo.Table_Document.IDCompany, dbo.Table_DocumentCategory.Name AS CategoryName, dbo.Table_Document.IDCategory, dbo.Table_Company.Name AS CompanyName, dbo.Table_Document.Name, 
                         dbo.Table_Document.Description, dbo.Table_Document.Flag, dbo.Table_Document.CreatedBy, dbo.Table_Document.CreatedAt
FROM            dbo.Table_DocumentCategory INNER JOIN
                         dbo.Table_Document ON dbo.Table_DocumentCategory.ID = dbo.Table_Document.IDCategory INNER JOIN
                         dbo.Table_Company ON dbo.Table_Document.IDCompany = dbo.Table_Company.ID
GO
SET IDENTITY_INSERT [dbo].[Table_Company] ON 

INSERT [dbo].[Table_Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (2, N'9841d43a-7e32-4eee-9dd1-5ef6e711cd8d', N'PT Indah Bersama', N'Kalimantan', N'mail@mail.me', N'082323196866', 1, 1, CAST(N'2023-08-03T12:20:33.290' AS DateTime))
INSERT [dbo].[Table_Company] ([ID], [UID], [Name], [Address], [Email], [Telephone], [Flag], [CreatedBy], [CreatedAt]) VALUES (3, N'5b5d6a3c-3494-4006-9633-cdd3d6098da5', N'PT Bakar Duit', N'Sleman', N'bakar@duit.net', N'082323198766', 1, 1, CAST(N'2023-08-03T16:16:14.910' AS DateTime))
SET IDENTITY_INSERT [dbo].[Table_Company] OFF
GO
SET IDENTITY_INSERT [dbo].[Table_Document] ON 

INSERT [dbo].[Table_Document] ([ID], [UID], [IDCompany], [IDCategory], [Name], [Description], [Flag], [CreatedBy], [CreatedAt]) VALUES (1, N'5aa62ceb-69e1-4781-815a-8337dc26a68b', 2, 2, N'Buku Hutang', N'Test', 2, 3, CAST(N'2023-08-03T15:45:35.160' AS DateTime))
SET IDENTITY_INSERT [dbo].[Table_Document] OFF
GO
SET IDENTITY_INSERT [dbo].[Table_DocumentCategory] ON 

INSERT [dbo].[Table_DocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (2, N'697e8caa-9984-440e-bd73-a8eefd12432e', N'Penting', 1, CAST(N'2023-08-03T15:44:12.687' AS DateTime))
INSERT [dbo].[Table_DocumentCategory] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (3, N'5efa29ab-8628-4d91-8984-efe4d3f6aa4f', N'Tambahan Edit', 1, CAST(N'2023-08-03T19:06:26.533' AS DateTime))
SET IDENTITY_INSERT [dbo].[Table_DocumentCategory] OFF
GO
SET IDENTITY_INSERT [dbo].[Table_Position] ON 

INSERT [dbo].[Table_Position] ([ID], [UID], [Name], [CreatedBy], [CreatedAt]) VALUES (1, N'ccd358eb-2c13-4aaf-b615-8230fe8d6d65', N'Manager', 1, CAST(N'2023-08-03T11:36:08.480' AS DateTime))
SET IDENTITY_INSERT [dbo].[Table_Position] OFF
GO
SET IDENTITY_INSERT [dbo].[Table_User] ON 

INSERT [dbo].[Table_User] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (2, N'63d066b3-5704-4c5d-851a-edd72006828c', 2, 1, N'Surya Aji Pratama', N'Bantul', N'suryaadjie08@gmail.com', N'082323196866', N'surya', N'AFF8FBCBF1363CD7EDC85A1E11391173', N'Admin', 1, 1, CAST(N'2023-08-03T16:13:44.130' AS DateTime))
INSERT [dbo].[Table_User] ([ID], [UID], [IDCompany], [IDPosition], [Name], [Address], [Email], [Telephone], [Username], [Password], [Role], [Flag], [CreatedBy], [CreatedAt]) VALUES (3, N'f105f370-b80d-4b10-bc42-5a0e64631053', 2, 1, N'alex', N'Sleman', N'a@mail.com', N'082323196866', N'alex', N'534B44A19BF18D20B71ECC4EB77C572F', N'Admin', 1, 1, CAST(N'2023-08-03T18:44:41.190' AS DateTime))
SET IDENTITY_INSERT [dbo].[Table_User] OFF
GO
ALTER TABLE [dbo].[Table_Document]  WITH CHECK ADD  CONSTRAINT [FK_Table_Document_Table_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Table_Company] ([ID])
GO
ALTER TABLE [dbo].[Table_Document] CHECK CONSTRAINT [FK_Table_Document_Table_Company]
GO
ALTER TABLE [dbo].[Table_Document]  WITH CHECK ADD  CONSTRAINT [FK_Table_Document_Table_DocumentCategory] FOREIGN KEY([IDCategory])
REFERENCES [dbo].[Table_DocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[Table_Document] CHECK CONSTRAINT [FK_Table_Document_Table_DocumentCategory]
GO
ALTER TABLE [dbo].[Table_DocumentCategory]  WITH CHECK ADD  CONSTRAINT [FK_Table_DocumentCategory_Table_DocumentCategory] FOREIGN KEY([ID])
REFERENCES [dbo].[Table_DocumentCategory] ([ID])
GO
ALTER TABLE [dbo].[Table_DocumentCategory] CHECK CONSTRAINT [FK_Table_DocumentCategory_Table_DocumentCategory]
GO
ALTER TABLE [dbo].[Table_User]  WITH CHECK ADD  CONSTRAINT [FK_Table_User_Table_Company] FOREIGN KEY([IDCompany])
REFERENCES [dbo].[Table_Company] ([ID])
GO
ALTER TABLE [dbo].[Table_User] CHECK CONSTRAINT [FK_Table_User_Table_Company]
GO
ALTER TABLE [dbo].[Table_User]  WITH CHECK ADD  CONSTRAINT [FK_Table_User_Table_Position] FOREIGN KEY([IDPosition])
REFERENCES [dbo].[Table_Position] ([ID])
GO
ALTER TABLE [dbo].[Table_User] CHECK CONSTRAINT [FK_Table_User_Table_Position]
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateCompany]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_CreateCompany] 
 -- Add the parameters for the stored procedure here 
  
 @name varchar(255),
 @address text,
 @email varchar(50),
 @telephone varchar(14),
 @flag int,
 @createdBy int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 INSERT INTO [dbo].[Table_Company] 
 ( 
  [UID], 
  [Name],
  [Address],
  [Email],
  [Telephone],
  [Flag],
  [CreatedBy],
  [CreatedAt]
 ) 
 VALUES 
 ( 
  NEWID(),  
  @name,
  @address,
  @email,
  @telephone,
  @flag,
  @createdBy,
  GETDATE() 
 ) 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocument]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_CreateDocument] 
 -- Add the parameters for the stored procedure here
 @idcompany int,
 @idcategory int,
 @name varchar(255),
 @description text,
 @flag int,
 @createdBy int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 INSERT INTO [dbo].[Table_Document] 
 ( 
  [UID],
  [IDCompany],
  [IDCategory],
  [Name],
  [Description],
  [Flag],
  [CreatedBy],
  [CreatedAt]
 ) 
 VALUES 
 ( 
  NEWID(),  
  @idcompany,
  @idcategory,
  @name,
  @description,
  @flag,
  @createdBy,
  GETDATE() 
 ) 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateDocumentCategory]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_CreateDocumentCategory] 
 -- Add the parameters for the stored procedure here 
  
 @name varchar(50),
 @createdBy int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 INSERT INTO [dbo].[Table_DocumentCategory] 
 ( 
  [UID], 
  [Name], 
  [CreatedBy],
  [CreatedAt]
 ) 
 VALUES 
 ( 
  NEWID(),  
  @name,
  @createdBy,
  GETDATE() 
 ) 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreatePosition]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_CreatePosition] 
 -- Add the parameters for the stored procedure here 
  
 @name varchar(50),
 @createdBy int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 INSERT INTO [dbo].[Table_Position] 
 ( 
  [UID], 
  [Name], 
  [CreatedBy],
  [CreatedAt]
 ) 
 VALUES 
 ( 
  NEWID(),  
  @name,
  @createdBy,
  GETDATE() 
 ) 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_CreateUser]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_CreateUser] 
 -- Add the parameters for the stored procedure here 
 @idCompany int,
 @idposition int,
 @name varchar(255), 
 @address text,
 @email varchar(50),
 @telephone varchar(14), 
 @username varchar(50),  
 @password varchar(255), 
 @role varchar(50),
 @flag int,
 @createdBy int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 INSERT INTO [dbo].[Table_User] 
 ( 
  [UID], 
  [IDCompany],
  [IDPosition],
  [Name], 
  [Address],
  [Email],
  [Telephone],  
  [Username], 
  [Password], 
  [Role],
  [Flag],
  [CreatedBy],
  [CreatedAt] 
 ) 
 VALUES 
 ( 
  NEWID(), 
  @idCompany,
  @idposition,
  @name, 
  @address,
  @email, 
  @telephone, 
  @username, 
  CONVERT(varchar(32), HASHBYTES('MD5', @password), 2), 
  @role,
  @flag,
  @createdBy,
  GETDATE() 
 ) 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteCompany]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_DeleteCompany] 
 -- Add the parameters for the stored procedure here
 @id int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 DELETE
 FROM
	[dbo].[Table_Company]
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocument]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_DeleteDocument] 
 -- Add the parameters for the stored procedure here
 @id int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 DELETE
 FROM
	[dbo].[Table_Document]
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteDocumentCategory]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_DeleteDocumentCategory] 
 -- Add the parameters for the stored procedure here
 @id int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 DELETE
 FROM
	[dbo].[Table_DocumentCategory]
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeletePosition]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_DeletePosition] 
 -- Add the parameters for the stored procedure here
 @id int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 DELETE
 FROM
	[dbo].[Table_Position]
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_DeleteUser]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_DeleteUser] 
 -- Add the parameters for the stored procedure here
 @id int,
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 DELETE
 FROM
	[dbo].[Table_User]
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_loginUser]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
CREATE PROCEDURE [dbo].[sp_loginUser]
	@email varchar(50),
	@password varchar(50),
	@retVal int OUTPUT

AS
BEGIN
SET NOCOUNT ON;
	SELECT
		[ID],
		[Username],
		[Email],
		[Role],
		[CreatedAt]
	FROM Table_User
	WHERE
		[Email] = @email and [Password] = CONVERT(varchar(32), HASHBYTES('MD5', @password), 2)


		IF(@@ROWCOUNT > 0)
		BEGIN
			SET @retVal = 200
		END
		ELSE
		BEGIN
			SET @retVal = 500
		END
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateCompany]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_UpdateCompany] 
 -- Add the parameters for the stored procedure here
 @id int, 
 @name varchar(255),
 @address text,
 @email varchar(50),
 @telephone varchar(14),
 @flag int,
 @createdBy int,  
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 UPDATE [dbo].[Table_Company] SET
   
  
  [Name] = @name,
  [Address] = @address,
  [Email] = @email,
  [Telephone] = @telephone,
  [Flag] = @flag,
  [CreatedBy] = @createdBy  
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocument]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_UpdateDocument] 
 -- Add the parameters for the stored procedure here
 @id int, 
 @idcompany int,
 @idcategory int,
 @name varchar(255),
 @description text,
 @flag int,
 @createdBy int,
 @retVal int OUTPUT
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 UPDATE [dbo].[Table_Document] SET
   
  [IDCompany] = @idcompany,
  [IDCategory] = @idcategory,
  [Description] = @description,
  [Flag] = @flag,
  [CreatedBy] = @createdBy  
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateDocumentCategory]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_UpdateDocumentCategory] 
 -- Add the parameters for the stored procedure here
 @id int, 
 @name varchar(50), 
 @createdBy int,  
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 UPDATE [dbo].[Table_DocumentCategory] SET
   
  
  [Name] = @name, 
  [CreatedBy] = @createdBy  
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdatePosition]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_UpdatePosition] 
 -- Add the parameters for the stored procedure here
 @id int, 
 @name varchar(50), 
 @createdBy int,  
 @retVal int OUTPUT 
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 UPDATE [dbo].[Table_Position] SET
   
  
  [Name] = @name, 
  [CreatedBy] = @createdBy  
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
/****** Object:  StoredProcedure [dbo].[sp_UpdateUser]    Script Date: 04/08/2023 08:56:22 ******/
SET ANSI_NULLS ON
GO
SET QUOTED_IDENTIFIER ON
GO
-- ============================================= 
-- Author:  <Author,,Name> 
-- Create date: <Create Date,,> 
-- Description: <Description,,> 
-- ============================================= 
CREATE PROCEDURE [dbo].[sp_UpdateUser] 
 -- Add the parameters for the stored procedure here
 @id int,
 @idCompany int,
 @idposition int,
 @name varchar(255), 
 @address text,
 @email varchar(50),
 @telephone varchar(14), 
 @username varchar(50),  
 @password varchar(255), 
 @role varchar(50),
 @flag int,
 @createdBy int,
 @retVal int OUTPUT  
AS 
BEGIN 
 -- SET NOCOUNT ON added to prevent extra result sets from 
 -- interfering with SELECT statements. 
 SET NOCOUNT ON; 
 
    -- Insert statements for procedure here 
 UPDATE [dbo].[Table_User] SET
   
  [IDCompany] = @idCompany,
  [IDPosition] = @idposition,
  [Name] = @name, 
  [Address] = @address,
  [Email] = @email,
  [Telephone] = @telephone,   
  [Username] = @username, 
  [Password] = CONVERT(varchar(32), HASHBYTES('MD5', @password), 2),
  [Role] = @role,
  [Flag] = @flag,
  [CreatedBy] = @createdBy
  
  WHERE ID = @id
  
 
 
 IF(@@ROWCOUNT > 0) 
 BEGIN 
  SET @retVal = 200 
 END 
 ELSE 
 BEGIN 
  SET @retVal = 500 
 END 
END
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Table_DocumentCategory"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Table_Document"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 5
         End
         Begin Table = "Table_Company"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 136
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_Document'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPane1', @value=N'[0E232FF0-B466-11cf-A24F-00AA00A3EFFF, 1.00]
Begin DesignProperties = 
   Begin PaneConfigurations = 
      Begin PaneConfiguration = 0
         NumPanes = 4
         Configuration = "(H (1[40] 4[20] 2[20] 3) )"
      End
      Begin PaneConfiguration = 1
         NumPanes = 3
         Configuration = "(H (1 [50] 4 [25] 3))"
      End
      Begin PaneConfiguration = 2
         NumPanes = 3
         Configuration = "(H (1 [50] 2 [25] 3))"
      End
      Begin PaneConfiguration = 3
         NumPanes = 3
         Configuration = "(H (4 [30] 2 [40] 3))"
      End
      Begin PaneConfiguration = 4
         NumPanes = 2
         Configuration = "(H (1 [56] 3))"
      End
      Begin PaneConfiguration = 5
         NumPanes = 2
         Configuration = "(H (2 [66] 3))"
      End
      Begin PaneConfiguration = 6
         NumPanes = 2
         Configuration = "(H (4 [50] 3))"
      End
      Begin PaneConfiguration = 7
         NumPanes = 1
         Configuration = "(V (3))"
      End
      Begin PaneConfiguration = 8
         NumPanes = 3
         Configuration = "(H (1[56] 4[18] 2) )"
      End
      Begin PaneConfiguration = 9
         NumPanes = 2
         Configuration = "(H (1 [75] 4))"
      End
      Begin PaneConfiguration = 10
         NumPanes = 2
         Configuration = "(H (1[66] 2) )"
      End
      Begin PaneConfiguration = 11
         NumPanes = 2
         Configuration = "(H (4 [60] 2))"
      End
      Begin PaneConfiguration = 12
         NumPanes = 1
         Configuration = "(H (1) )"
      End
      Begin PaneConfiguration = 13
         NumPanes = 1
         Configuration = "(V (4))"
      End
      Begin PaneConfiguration = 14
         NumPanes = 1
         Configuration = "(V (2))"
      End
      ActivePaneConfig = 0
   End
   Begin DiagramPane = 
      Begin Origin = 
         Top = 0
         Left = 0
      End
      Begin Tables = 
         Begin Table = "Table_Company"
            Begin Extent = 
               Top = 6
               Left = 38
               Bottom = 136
               Right = 208
            End
            DisplayFlags = 280
            TopColumn = 1
         End
         Begin Table = "Table_User"
            Begin Extent = 
               Top = 6
               Left = 246
               Bottom = 136
               Right = 416
            End
            DisplayFlags = 280
            TopColumn = 10
         End
         Begin Table = "Table_Position"
            Begin Extent = 
               Top = 6
               Left = 454
               Bottom = 136
               Right = 624
            End
            DisplayFlags = 280
            TopColumn = 0
         End
      End
   End
   Begin SQLPane = 
   End
   Begin DataPane = 
      Begin ParameterDefaults = ""
      End
   End
   Begin CriteriaPane = 
      Begin ColumnWidths = 11
         Column = 1440
         Alias = 900
         Table = 1170
         Output = 720
         Append = 1400
         NewValue = 1170
         SortType = 1350
         SortOrder = 1410
         GroupBy = 1350
         Filter = 1350
         Or = 1350
         Or = 1350
         Or = 1350
      End
   End
End
' , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
EXEC sys.sp_addextendedproperty @name=N'MS_DiagramPaneCount', @value=1 , @level0type=N'SCHEMA',@level0name=N'dbo', @level1type=N'VIEW',@level1name=N'View_User'
GO
USE [master]
GO
ALTER DATABASE [PretestSurya] SET  READ_WRITE 
GO
