﻿using Dapper;
using PretestCoreSurya.Model;

namespace PretestCoreSurya.Repository.Interface
{
    public interface IDocument
    {
        Response<T> Execute_Command<T>(string query, DynamicParameters sp_params);
        Response<List<T>> getDocumentList<T>();
    }
}
