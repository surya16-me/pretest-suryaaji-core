﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreSurya.Model
{
    public class ModelPosition
    {
        [Required]
        public string? Name { get; set; }

        [Required]
        public int? CreatedBy { get; set; }
    }
}
