﻿using System.ComponentModel.DataAnnotations;

namespace PretestCoreSurya.Model
{
    public class ModelDocumentCategory
    {
        [Required]
        public string? Name { get; set; }

        [Required]
        public int? CreatedBy { get; set; }
    }
}
